package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"time"

	"gitlab.com/tzsuite/rpc"
	"gitlab.com/tzsuite/tzutil"
)

var seed = flag.String("seed", "", "specify a seed for the keys that will fund the setup")

var rpcURL = flag.String("rpcurl", "", "specify the base url of the tezos-node RPC")

func main() {
	flag.Parse()

	ch := make(chan error)
	shutdown := make(chan struct{})

	r, err := rpc.New(rpc.ConfAlphanet(*rpcURL), nil, shutdown, ch)
	if err != nil {
		log.Fatalf("setting up RPC client: %v", err)
	}

	go func(chan struct{}) {
		setup(r)
		close(shutdown)
	}(shutdown)

	for {
		select {
		case _, more := <-shutdown:
			if !more {
				time.Sleep(time.Millisecond * 500)
				return
			}
		case err := <-ch:
			fmt.Println("error from goroutine:", err)
			close(shutdown)
		}
	}
}

func setup(r *rpc.RPC) {
	moneySeed := *seed
	moneyKeys, err := tzutil.KeysFromStringSeed(moneySeed)
	if err != nil {
		log.Fatalf("regenerating money keys: %v", err)
	}

	oracleKeys, err := tzutil.KeysWithoutSeed()
	if err != nil {
		log.Fatalf("generating oracle keys: %v", err)
	}

	providerKeys, err := tzutil.KeysWithoutSeed()
	if err != nil {
		log.Fatalf("generating oracle keys: %v", err)
	}

	fmt.Printf("Oracle Identity: %#v\n", oracleKeys)
	fmt.Printf("Provider Identity: %#v\n", providerKeys)

	done, err := r.BatchTransfer(moneyKeys, 10000000, moneyKeys.PKH, oracleKeys.PKH, nil)
	if err != nil {
		log.Fatal(err)
	}
	go func(done <-chan string) {
		opHash := <-done
		fmt.Println("transfer to oracle injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("transfer to oracle included in block:", blockHash)
	}(done)

	done, err = r.BatchTransfer(moneyKeys, 10000000, moneyKeys.PKH, providerKeys.PKH, nil)
	if err != nil {
		log.Fatal(err)
	}
	go func(done <-chan string) {
		opHash := <-done
		fmt.Println("transfer to provider injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("transfer to provider included in block:", blockHash)
	}(done)

	// oracle contract
	done, oracleDone, err := r.BatchOriginate(moneyKeys, oracleKeys.PKH, 0, moneyKeys.PKH, prepareOracleScript(oracleKeys.PKH), false, false, "")
	if err != nil {
		log.Fatal(err)
	}
	included := make(chan string)
	go func(done <-chan string, included chan<- string) {
		opHash := <-done
		fmt.Println("origination of oracle contract injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("origination of oracle contract included in block:", blockHash)
		included <- blockHash
	}(done, included)

	// placeholder proxy contract
	done, placeholderDone, err := r.BatchOriginate(moneyKeys, providerKeys.PKH, 0, moneyKeys.PKH, json.RawMessage(placeholderScript), false, false, "")
	if err != nil {
		log.Fatal(err)
	}
	go func(done <-chan string, included chan<- string) {
		opHash := <-done
		fmt.Println("origination of placeholder proxy contract injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("origination of placeholder proxy contract included in block:", blockHash)
		included <- blockHash
	}(done, included)

	oracle := <-oracleDone
	placeholder := <-placeholderDone
	// make sure both originations made it into a block before continuing
	<-included
	<-included

	fmt.Println("oracle address is:", oracle)
	fmt.Println("placeholder address is:", placeholder)

	// provider contract
	// we need the previous two contracts for this one
	done, providerDone, err := r.BatchOriginate(moneyKeys, providerKeys.PKH, 20000000, moneyKeys.PKH, prepareProviderContract(providerKeys.PublicStr, oracle, placeholder), false, false, "")
	if err != nil {
		log.Fatal(err)
	}
	go func(done <-chan string, included chan<- string) {
		opHash := <-done
		fmt.Println("origination of provider contract injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("origination of provider contract included in block:", blockHash)
		included <- blockHash
	}(done, included)

	provider := <-providerDone
	<-included

	fmt.Println("provider address is:", provider)

	// real proxy contract
	// we need the oracle PKH and provider contract for this one
	done, proxyDone, err := r.BatchOriginate(moneyKeys, providerKeys.PKH, 0, moneyKeys.PKH, prepareProxyContract(oracleKeys.PKH, provider), false, false, "")
	if err != nil {
		log.Fatal(err)
	}
	go func(done <-chan string, included chan<- string) {
		opHash := <-done
		fmt.Println("origination of real proxy contract injected in operation:", opHash)
		blockHash := <-done
		fmt.Println("origination of real proxy contract included in block:", blockHash)
		included <- blockHash
	}(done, included)

	proxy := <-proxyDone
	<-included

	_, sig, err := r.HashAndSignData(prepareHDI(proxy), providerKeys)
	if err != nil {
		log.Fatal(err)
	}
	// initialize the provider contract
	// we need to sign the proxy contract address and send it along with the signature
	done, err = r.BatchTransfer(moneyKeys, 10000000, moneyKeys.PKH, provider, initParams(proxy, sig))
	if err != nil {
		log.Fatal(err)
	}
	opHash := <-done
	fmt.Println("initialization of provider injected in operation:", opHash)
	blockHash := <-done
	fmt.Println("initialization of provider included in block:", blockHash)

	fmt.Println("It works.")
	fmt.Printf("Oracle Identity: %#v\n", oracleKeys)
	fmt.Printf("Oracle Contract: %v\n", oracle)
	fmt.Printf("Provider Identity: %#v\n", providerKeys)
	fmt.Printf("Provider Contract: %v\n", provider)
	fmt.Printf("Proxy Contract: %v\n", proxy)

	close(included)
}

func prepareOracleScript(oraclePKH string) json.RawMessage {
	return json.RawMessage(oracleScript + oracleScriptInitStorage(oraclePKH))
}

func prepareProviderContract(providerPubKey, oracleContract, placeholderContract string) json.RawMessage {
	return json.RawMessage(providerScript + providerScriptInitStorage(providerPubKey, oracleContract, placeholderContract))
}

func prepareProxyContract(oraclePKH, providerContract string) json.RawMessage {
	return json.RawMessage(proxyScript + proxyScriptInitStorage(oraclePKH, providerContract))
}

func prepareHDI(proxyContract string) rpc.HashDataInput {
	return rpc.HashDataInput{
		Data: json.RawMessage(fmt.Sprintf(`{"string":"%s"}`, proxyContract)),
		Type: json.RawMessage(`{"prim":"contract","args":[{"prim":"pair","args":[{"prim":"bytes"},{"prim":"nat"}]}]}`),
	}
}

func initParams(proxyContract, sig string) json.RawMessage {
	return json.RawMessage(fmt.Sprintf(`{"prim":"Left","args":[{"prim":"Right","args":[{"prim":"Right","args":[{"prim":"Right","args":[{"prim":"Pair","args":[{"string":"%s"},{"string":"%s"}]}]}]}]}]}`, sig, proxyContract))
}
